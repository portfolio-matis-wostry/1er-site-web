# Mon 1er site web

Ce site web est issu d'un projet de cours où nous devions créer un blog afin de démontrer les premières compétences acquises lors de nos premiers mois d'apprentissage.

J'ai choisi de créer un blog sur le thème du magasin de basket-ball B-Shop situé à Lille, car il allie ma passion pour le basket et celle pour les chaussures.

Ce site n'est pas encore tout à fait terminé car les pages "Blogs" et "Articles" ne sont pas encore créées.
