/* eslint-disable require-jsdoc */
/* Menu burger */
const burger = document.querySelector('header>p');
const burgerAside = document.querySelector('aside');
const croix = document.querySelector('aside>nav>p');

burger.addEventListener('click', () =>{
  if (burger.textContent == '\u2630') {
    burgerAside.setAttribute('class', 'visible');
  }
});

croix.addEventListener('click', () =>{
  if (croix.textContent == '\u2573') {
    burgerAside.removeAttribute('class');
  }
});

/* Carrousel */
const pre = document.querySelector('main>article>div>p:nth-of-type(1)');
const suiv = document.querySelector('main>article>div>p:nth-of-type(2)');
const spans = document.querySelectorAll('main>article>div>div>span');
const diapos = document.querySelectorAll('main>article>div>img');

let index = 0;

// Click sur le bouton suiv
suiv.addEventListener('click', () => {
  // Ce que l'on fait
  index += 1;
  if (index === diapos.length) {
    index = 0;
  }

  afficheDiapo(index);
});

// Click sur le bouton pre
pre.addEventListener('click', () => {
  // Ce que l'on fait
  index -= 1;
  if (index === -1) {
    index = diapos.length - 1;
  }

  afficheDiapo(index);
});

// Gestion des spans
spans.forEach((span, j) => {
  // Ce que l'on fait avec une span
  // Click sur la span
  span.addEventListener('click', () => {
    // Ce que l'on fait
    index = j;

    afficheDiapo(index);
  });
});

// Fonction afficher une diapo
function afficheDiapo(indexDiapo) {
  // Ce que fait la fonction
  // Effacer toutes les images
  diapos.forEach((diapo) => {
    diapo.classList.remove('display');
  });
  diapos[indexDiapo].classList.add('display');

  // Gestion des spans
  spans.forEach((span, i) => {
    if (i === indexDiapo) {
      span.classList.add('action');
    } else if (i !== indexDiapo) {
      span.classList.remove('action');
    }
  });
}

const currentIndex = 0;
let intervalId;
const isPaused = false;

function slideShow() {
  images[currentIndex].classList.remove('active');
  currentIndex = (currentIndex + 1) % images.length;
  images[currentIndex].classList.add('active');
}

// eslint-disable-next-line no-unused-vars
function pauseSlideshow() {
  if (isPaused) {
    intervalId = setInterval(slideShow, 3000);
    document.getElementById('pauseButton').innerHTML = 'Pause';
    isPaused = false;
  } else {
    clearInterval(intervalId);
    document.getElementById('pauseButton').innerHTML = 'Play';
    isPaused = true;
  }
}

intervalId = setInterval(slideShow, 3000);

